'''
Template Component main class.

'''
import csv
import os
import logging
from datetime import datetime
import pytz

from keboola.component.base import ComponentBase
from keboola.component.exceptions import UserException

# configuration variables
KEY_API_TOKEN = '#api_token'
KEY_PRINT_HELLO = 'print_hello'

# list of mandatory parameters => if some is missing,
# component will fail with readable message on initialization.
REQUIRED_PARAMETERS = [KEY_PRINT_HELLO]
REQUIRED_IMAGE_PARS = []


class Component(ComponentBase):
    """
        Extends base class for general Python components. Initializes the CommonInterface
        and performs configuration validation.

        For easier debugging the data folder is picked up by default from `../data` path,
        relative to working directory.

        If `debug` parameter is present in the `config.json`, the default logger is set to verbose DEBUG mode.
    """

    def __init__(self):
        super().__init__()

    def run(self):
        '''
        Main execution code
        '''

        # DATA_FOLDER = self.data_folder_path

        last_state = self.get_state_file()
        print(last_state.get('last_updated', ''))
        print('version 0.4.4')

        in_table_defs = self.get_input_tables_definitions()
        SOURCE_FILE_PATH = in_table_defs[0].full_path

        RESULT_FILE_PATH = os.path.join(self.tables_out_path, 'output.csv')

        config = self.configuration.parameters
        PARAM_PRINT_LINES = config['print_rows']

        print('Running...')
        with open(SOURCE_FILE_PATH, 'r') as input, open(RESULT_FILE_PATH, 'w+', newline='') as out:
            reader = csv.DictReader(input)
            new_columns = reader.fieldnames
            # append row number col
            new_columns.append('row_number')
            writer = csv.DictWriter(out, fieldnames=new_columns, lineterminator='\n', delimiter=',')
            writer.writeheader()
            for index, l in enumerate(reader):
                # print line
                if PARAM_PRINT_LINES:
                    print(f'Printing line {index}: {l}')
                # add row number
                l['row_number'] = index
                writer.writerow(l)

        table = self.create_out_table_definition(RESULT_FILE_PATH, incremental=True, primary_key=['row_number'])
        self.write_manifest(table)

        now = datetime.now(pytz.timezone('Europe/Prague'))
        self.write_state_file({'last_updated': str(now)})
        print(str(now))


"""
        Main entrypoint
"""
if __name__ == "__main__":
    try:
        comp = Component()
        # this triggers the run method by default and is controlled by the configuration.action parameter
        comp.execute_action()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
